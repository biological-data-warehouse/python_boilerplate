# Installation

## Stable release

To install Python Boilerplate, run this command in your terminal:

```bash
conda install python_boilerplate -c biological-data-warehouse
```

This is the preferred method to install Python Boilerplate, as it will always install the most recent stable release.

If you don't have [conda] installed, this [Python installation guide] can guide
you through the process.

[conda]: https://conda.io
[Python installation guide]: https://conda.io/docs/user-guide/install/index.html

## From sources

The sources for Python Boilerplate can be downloaded from the [GitLab repo].

You can either clone the public repository:

```bash
git clone git://gitlab.com/biological-data-warehouse/python_boilerplate
```

Or download the [tarball]:

```bash
curl -OL https://gitlab.com/biological-data-warehouse/python_boilerplate/repository/master/archive.tar
```

Once you have a copy of the source, you can install it with:

```bash
python setup.py install
```

[GitLab repo]: https://gitlab.com/biological-data-warehouse/python_boilerplate
[tarball]: https://gitlab.com/biological-data-warehouse/python_boilerplate/repository/master/archive.tar
